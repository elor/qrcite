#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 31 13:27:19 2022

@author: @M@nerdculture.de
"""

import argparse
import bibtexparser
import qrcode
import re

### Handling Parameters
parser = argparse.ArgumentParser(description='Handle parameters')
parser.add_argument("-i","--input", action="store", default='Examples.bib', dest="file", help="Input file name")
parser.add_argument("-s","--sci-hub", action="store_true",dest="sci", help="Change Reference Link to shadow library")
parser.add_argument("-c","--color", action="store", default='#000000', dest="qrcolor", help="Color of QR pixels")
parser.add_argument("-t","--type", action="store", default="png", dest="filetype", help="output type, png is default")
args = parser.parse_args()

### Definitions
qrPixelSize = 20
qrBorderSize = 1
#sci = True
refstring = "https://doi.org/"
if args.sci:
    refstring = "https://sci-hub.se/"

### Open the bib file
file = args.file #'Examples.bib' as default
with open(file) as bibtex_file:
    bib_database = bibtexparser.load(bibtex_file)

### Iterate through all entries in the file
for entry, key in zip(bib_database.entries, bib_database.entries_dict):
    ## Extract DOI
    doi = bib_database.entries[0]['doi']

    ## Check that it's just the doi and not the whole link:
    doi = re.sub(r'^(https?://)?(dx.|www.)?doi.org/', '', doi)

    ### make it a link
    link = refstring + doi #Example: "https://doi.org/10.1002/andp.19053220607"

    ### Create QR code
    qr = qrcode.QRCode(
        version=1, #number from 1-40, corresponds to size
        error_correction=qrcode.constants.ERROR_CORRECT_L, #L 7%, M 15%, Q 25%, H 30 % of errors can be corrected
        box_size=qrPixelSize, #Pixelsize of one dot
        border=qrBorderSize, #Free space around
    )
    qr.add_data(link) #text to qr
    qr.make()   #fit=True determines size automatically
    img = qr.make_image(fill_color=args.qrcolor, back_color="white")

    #img.show() #show output in Console; however not in spider - but in external viewer
    img.save(key + "." + args.filetype) #png, pdf 
    
    